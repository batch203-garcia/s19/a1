//console.log("Hello World!")

let username, password, role;

function login() {

    let usernamePromt = prompt("Enter your Username: ");
    username = usernamePromt;
    let passwordPromt = prompt("Enter your Password: ");
    password = passwordPromt;
    let rolePromt = prompt("Enter your role: ").toLowerCase();
    role = rolePromt;

    if (username == "" || username == null || password == "" || password == null || role == "" || role == null) {
        alert("Input should not be empty.");
    } else {
        switch (role) {
            case "admin":
                alert("Welcome back to the class portal, admin!");
                break;
            case "teacher":
                alert("Thank you for logging in, teacher!");
                break;
            case "student":
                alert("Welcome to the class portal, student!");
                break;
            default:
                alert("Role out of range.");
                break;
        }
    }
}

login();

function checkAverage(grade1, grade2, grade3, grade4) {
    function avg() {
        let avg = (grade1 + grade2 + grade3 + grade4) / 4
        if (avg <= 74) {
            console.log("Hello, student, your average is " + Math.round(avg) + "." + " The letter equivalent is F")
        } else if (avg >= 75 && avg <= 79) {
            console.log("Hello, student, your average is " + Math.round(avg) + "." + " The letter equivalent is D")
        } else if (avg >= 80 && avg <= 84) {
            console.log("Hello, student, your average is " + Math.round(avg) + "." + " The letter equivalent is C")
        } else if (avg >= 85 && avg <= 89) {
            console.log("Hello, student, your average is " + Math.round(avg) + "." + " The letter equivalent is B")
        } else if (avg >= 90 && avg <= 95) {
            console.log("Hello, student, your average is " + Math.round(avg) + "." + " The letter equivalent is A")
        } else if (avg > 96) {
            console.log("Hello, student, your average is " + Math.round(avg) + "." + " The letter equivalent is A+")
        }
    }
    avg();
}